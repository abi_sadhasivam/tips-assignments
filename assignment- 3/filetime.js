/* global console:true */
/* global require:true */
/* global prompt:true */
/* global module:true */
var fs = require('fs');
var path = require('path');
var n;
fs.readFile('./config.txt', function (err, data) {
  if (err) throw err;
	fileOperations(data);
	});

var fileOperations = function (data) {
	var fileData = data.toString(),destination;
	var srcDest = fileData.split(',');
	for (var i = 0; i < srcDest.length; i++) {
		if(srcDest[i] == "destination") {
			destination = srcDest.splice(i);
		}
	}
	try {
		if(srcDest.length!== destination.length ) {
			throw "Each Source file should have a Destination path. Check the config file";
		}
	}
	catch (err){
		console.log(err);	
	}
	for(i = 1;i < srcDest.length;i++) {
		checkDirectory(srcDest[i],destination[i]);
		
	}
};

var checkDirectory = function (src , dest) {
	console.log(dest.toString());
	fs.exists(dest.toString(), function (exists) {
		try {
			if(exists === true){
//if directory exists
				copyFile(src.toString(),dest.toString());
			} else {
//if the directory doesnt exists create
				fs.mkdir(dest.toString(), function(err) {
					if (err) {
						throw err;
					} else{
						copyFile(src.toString(),dest.toString());
					}
				});
			}
		} 
		catch(err){
			console.log(err);
		}
	});
}; 
var copyFile = function (src,dest) {
	fs.stat(src.toString(),function(err, stats) {
		if(stats.isDirectory() === true) {
			//Checking if the source is directory- loop to copy
			fs.readdir(src.toString(), function(err, files) {
				if(err) {
					console.log("Error : " + err);
					return;
				}	else{
					for (i=0; i<files.length; i++) {
						var fileName = src.toString()+'\\'+files[i];
						console.log(fileName);
						var time = process.hrtime();
						var read = fs.createReadStream(fileName);
						var write = fs.createWriteStream(dest.toString()+'\\'+files[i], {'flags': 'a'});
						read.pipe(write);
						var diff = process.hrtime(time);
						console.log("time dif" +diff[1]+"ns")
						fs.appendFile('config2.txt', "\nTime Taken"+diff[1]+'ns', function (err) {
							if(err) {
								console.log(err);
							}
						});
						
						}
					return;
				}
			});	
		} else {
				var fileName = src.toString();
				console.log(fileName);
				var destName = path.basename(fileName)// To get the file name from the path specified
				var time = process.hrtime();
				var read = fs.createReadStream(fileName);
				var write = fs.createWriteStream(dest.toString()+'\\'+destName, {'flags': 'a'});
				read.pipe(write);
				write.on("error", function(err) {
				console.log(err);      
				});
				var diff = process.hrtime(time);
				console.log("time dif" +diff[1]+"ns")
				fs.appendFile('config2.txt', "\nTime Taken"+diff[1]+'ns', function (err) {
					if(err) {
						console.log(err);
					}
				});
				
				return;
			}
	});
};
	
//	console.log("srcprint \n"+fileDataSplit);
