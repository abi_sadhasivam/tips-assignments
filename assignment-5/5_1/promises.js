/* global require : true */
/* global console : true */
var fs = require ('fs');
var http = require('http');
var q = require('q');
var current= 0, perc = 0;
var fileDownload = "http://nodejs.org/dist/v0.12.4/node.exe";
var down = function () {
	var promise = download().then (function (msg) {
		console.log(msg);
		},function (err) {
		console.error(err);
	});
};

var download = function () {
	var deferred = q.defer();
	var node = fs.createWriteStream ('node.exe');
	var request = http.get(fileDownload, function(response){
		var contentType = response.headers;
		var length = response.headers['content-length'];
		var total= length ;
		response.pipe(node); //writing the data
		response.on("data", function(chunk) {            
			current += chunk.length;
			perc = (100.0 * (current / total)) + "% " ;//percentage
			console.log(perc);
			if(current == total){
				deferred.resolve ("The Download is complete");			
			}
		});
		response.on("error", function(err) {            
			deferred.reject(err);
		});
	});
	return deferred.promise;
};
down();
//9388416