/* global require : true */
/* global console : true */
var fs = require ('fs');
var http = require('http');
var q = require('q');
var emit = require('events');
var EventEmitter = require("events").EventEmitter;
var current= 0, perc = 0;
var fileDownload = "http://nodejs.org/dist/v0.12.4/node.exe";
var ee = new EventEmitter();
var download = function () {
	var node = fs.createWriteStream ('node.exe');
	var request = http.get(fileDownload, function(response){
		var contentType = response.headers;
		var length = response.headers['content-length'];
		var total= length ;
		response.pipe(node); //writing the data
		
		response.on("data", function(chunk) {            
			current += chunk.length;
			perc = (100.0 * (current / total)) + "% " ;//percentage
			console.log(perc);
			if(current == total){
				
				ee.emit("complete");			
			}
		});
		response.on("error", function(err) {            
			ee.emit("err");
			ee.emit("err");
		});
	});
};
download();
ee.on("complete", function () {
	console.log("the download is complete");
});
ee.on("error", function () {
	console.log("the download is incomplete");
});