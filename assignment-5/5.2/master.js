/* global require:true */
/* global console:true */
var process = require('child_process');
var timer = require('timers');
var q = require('q');
var child1 = process.exec('./child1.js').exec;
var child2 = process.exec('./child2.js').exec;
var choice1, choice2,wins;
var start = function() {
	var promise = compare().then (function(arr) {
		choice1 = arr[0].trim();
		choice2 = arr[1].trim();
		if(choice1 === choice2) {
			wins= "The result is a tie!";
		}
		if(choice1 === "rock") {
				if(choice2 === "scissor") {
						wins = "player1 wins";
				} else {
					if(choice2 === "paper") {
						wins ="player2 wins";
					}
			}
		}
		if(choice1 === "paper") {
				if(choice2 === "rock") {
						wins = "player1 wins";
				} else {
						if(choice2 === "scissor") {
								wins = "player2 wins";
					}
			}
		}
		if(choice1 === "scissor") {
				if(choice2 === "rock") {
						wins = "Player 2 wins";
				} else {
						if(choice2 === "paper") {
								wins = "Player 1 wins";
						}
				}
		}
		console.log(wins);
	},function (err) {
		console.error(err);
	});
}; 
																								
var compare = function() {	
	var deferred = q.defer();
	process.exec('node ./child1.js', function(error, stdout, stderr) {
		choice1= stdout;	
		console.log('stdout1: ', stdout);
	});
	process.exec('node ./child2.js', function(error, stdout, stderr) {
		choice2 = stdout;
		console.log('stdout2: ', stdout);
	});
	timer.setTimeout (function () {
		if(choice1.length >0 &&choice2.length >0){
			var arr= [choice1, choice2];
			deferred.resolve (arr);		
		}
		else
		{
			deferred.reject("Child Process havent occured yet");
		}
	},1500);
	return deferred.promise;
};

timer.setInterval (function () {
	start();
},3000);