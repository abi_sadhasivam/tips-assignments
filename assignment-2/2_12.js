//Adding method to Number Object
var numberInherit = function () {
	var that= {};
	that.divide= function(num1,num2) {
		console.log("The divide value is" + (num1/num2));
	};
	return that;
};

var numberCheck = numberInherit();
Number.prototype.check = numberCheck;
var num = 5;
num.check.divide(4,5);


//Adding methods to a String object
var stringInherit = function () {
	var that= {};
	that.concat = function(string1,string2) {
		console.log("The concated value : " + string1 +string2);
	};
	return that;
};

var stringCheck = stringInherit();
String.prototype.check = stringCheck;
var string= "";
string.check.concat('hello','world');