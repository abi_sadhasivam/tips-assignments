//Prototypal and Functionals
//------------------------------------------Parent Class-Shape----------------------------------------------------
var shape = {
	getShape : function(name) {
		console.log("The Shape is" +name);
	}
};
//fucntional inheritance for shape
var shape2 = function(){
	var that = {};
	that.getShape = function(name) {
		console.log("Executing with functional inheritance");
		console.log("The Shape is " + name);
	};
	return that;
};
//----------------------------------------------Parent Class- Line------------------------------------------------
var line = {
	show : function()	{
		console.log("Inheriting the line class");
	}
};
//----------------------------------------------Child Class-Rectangle---------------------------------------------
var rectangle =Object.create(shape);	
rectangle.getShape('Rectangle');
rectangle.area = function (input1,input2,shape) {
	if(shape === "rectangle"){
	console.log("area of Rectangle: " + (input1 * input2));
	}	else	{
		console.log("area of Rhombus: " +((input1*input2)/2));
	}
};
rectangle.area(5,6,'rectangle');
//-----------------------------------------------Child class of shape-Square----------------------------------
var square = Object.create(shape);
square.getShape('Square');
square.area = function (input){
	console.log("Area of Sqaure" + (input * input));
};
square.area(5);

//-----------------------Rhombus using functional------------
var rhombus = function () {
	var that = shape2();
	that.area = function (input1,input2)
	{
		console.log("Area of REctangle : " + ((input1*input2)/2));
	};
	return that;
};
var myRhombus = rhombus();
myRhombus.getShape("Rhombus");
myRhombus.area(2,3);

