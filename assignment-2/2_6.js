var prompt=require('prompt');
var prop;
var operation= {
	num1: 6,
	num2: 9,
	add : function(num1,num2) {
		var inneradd = function() {
			console.log("Addition value is "+ (num1+num2));
		};
		inneradd();//function invocation
	},
	sub : function(num1,num2) {
		var innersub = function() {
			console.log("Subtracted value is" + (num1-num2));
		};
		innersub();
	},
	multiply : function (num1, num2) {
		var innermultiply = function() {
			console.log("Multiplied value is" + (num1*num2));
		};
		innermultiply();
	},
	divide : function (num1,num2)	{
		var innerdiv = function(){
			console.log("Divided value is  " + (num1/num2));
		};
		innerdiv();
	}
};
prompt.start();
var menu = function () {
	prompt.get(['number1','number2'],function (err, result) { 
		try{
				if(isNaN(result.number1) || isNaN(result.number2)) {
					throw "Enter a valid number";
				} else	{
					for(prop in operation) {
						if(typeof operation[prop] === 'function')	{
							operation[prop](result.number1-0,result.number2-0);// method invocation
						}
					}
				}
			}
		catch(err) {
			console.log(err);
			menu();
		}
	});
}
menu();