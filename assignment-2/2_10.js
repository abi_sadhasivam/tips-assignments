//Pseudoclassical
//------------------------------------------Parent Class-------------------------------------------------------
var shape = function () {
};
//-----------------------------------------parent class function-----------------------------------------------
shape.prototype.getShape=function(name) {
	console.log(name);
};
//-------------------------------------------Parent Class- Line-----------------------------------------------
var line = function() {
};
line.prototype.show = function () {
	console.log("Inheriting the line");
};
//---------------------------------------------------Child class-----------------------------------------------
var rectangle = function() { 
};
rectangle.prototype = new shape();
rectangle.prototype.getShape("Rectangle");
//child Class to calculate area
rectangle.prototype.area=function(input1,input2,shape) {
	if(shape === "rectangle"){
	console.log("area of Rectangle: " + (input1 * input2));
	}	else	{
		console.log("area of Rhombus: " +((input1*input2)/2));
	}
};
var myrect = new rectangle();
myrect.prototype= new line ();
//------------------------------------Overriding the function in line parent-----------------------------------
myrect.prototype.show = function() {
	console.log("Inheriting");
};
myrect.prototype.show();
var rectArea = myrect. area(5,6,'rectangle');
//----------------------------------------------child of rectangle---------------------------------------------
var rhombus = function(){
};
rhombus.prototype = new rectangle();
rhombus.prototype.getShape("Rhombus");
var myrhombus = new rhombus();
var rhomusArea = myrhombus.area(4,5,'rhombus');
//---------------------------------------------Child class of shape-Square-----------------------------------
var square = function() {
};
square.prototype = new shape();
square.prototype.getShape("Rhombus");
square.prototype.area = function (input) {
	console.log("Area of Square: "+(input*input)); 
};  
var mySquare = new square();
var squareArea = mySquare.area(4);
