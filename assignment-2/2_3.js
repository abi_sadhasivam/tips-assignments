//Enumerate the values of the camera that are of number type

var camera = { 
	pixels: 40,
	color : "white",
	manufacturer : "Sony",
	series :"Z series",
  capturePicture	: function() {
	},
	viewPicture : function() {
	},
	recordVideo : function() {
	}
};
var property;
var print = function() {
	for(property in camera)
	{
		if(typeof camera[property] === 'number')
		{
			console.log(property + ': ' + camera[property]);
		}
	}
};
print();