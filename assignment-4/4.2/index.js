/*global console :true*/
/*global require :true*/
var util = require('./util.js');
//var a;// Declared without any specific datatype
var obj = {
	prop :"1"
};
var func = function () {
	try {
		throw("Welcome guest!");
	}	catch(err) {
		console.log(err);
	}
};
var arr = [1, 2, 3];
//var num = null;
var bool = false;
var string = "This s a string";
var error = new Error("The error message");
var funcCall = function (obj) {
	console.log("\nThe Results are");
	console.log("isUndefined : " +util.isUndefined(obj));
	console.log("isNull : " +util.isNull(obj));
	console.log("isNaN : " +util.isNaN(obj)) ;
	console.log("isError"+util.isError(obj)) ;
	console.log("isBoolean : " +util.isBoolean(obj)) ;
	console.log("isNumber : " +util.isNumber(obj)) ;
	console.log("isFunction : " +util.isFunction(obj)) ;
	console.log("isObject: " +util.isObject(obj) );
	console.log("isArray : " +util.isArray(obj)) ;
};
funcCall(error);
funcCall(arr);
//funcCall(num);
funcCall(bool);
funcCall(string);
funcCall(obj);