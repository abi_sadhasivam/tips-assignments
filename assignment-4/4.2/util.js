/*global module :true*/
/*global console :true*/
var isUndefined = function(a) {
	if(a === undefined) {
		return true;
	}
	return false;
};
var isNull = function(obj) {
	if(obj === null) {
		return true;
	}
	return false;
};
var isNAN = function (obj) {
	return isNaN(obj);
};
var isError = function(obj) {
	if (obj.constructor === Error){
		return true;
	}
	return false;
};
var IsBoolean = function (obj) {
	return Boolean(obj);
};
var isNumber = function (obj) {
	return( !isNaN (obj) && obj !== null && obj !== "" && obj !== false);
};
var isString = function(obj) {
	if( obj instanceof String || typeof(obj === String)){
	return true;
	}
	return false;
};
var isFunction = function(obj) {
	if(typeof(obj) ==="function"){
		return true;
	}
	return false;
};
var isObject = function(obj) {
	if(typeof(obj) ==="object"){
		return true;
	}
	return false;
};
var isArray = function(obj) {
	if(obj instanceof Array){
		return true;
	}
	return false;
};
module.exports.isUndefined = isUndefined;
module.exports.isNull = isNull;
module.exports.isNaN = isNaN;
module.exports.isError = isError;
module.exports.isBoolean = IsBoolean;
module.exports.isNumber = isNumber;
module.exports.isString = isString;
module.exports.isFunction = isFunction;
module.exports.isObject = isObject;
module.exports.isArray = isArray;