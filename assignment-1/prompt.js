/* global console:true */
/* global require:true */
/* global module:true */
var prompt = require('prompt');
var length = 0,
		bookObj = [	
			{"author" :"sidney","books"  : ["rage of angels","if tomorrow comes"]},
			{"author" :"rowling","books" :["harry potter"]},
			{"author" :"hilary ","books" :["Wolf hall"]},	
			{"author" :"Chetan","books"  :["One night at call center","revolution 20-20"]} ];
   
var displayMenu =function () {
	console.log('Input the assignment number- 1/2/3/4/5');
	prompt.start(); 
	//asks the user to input the assignment number
	prompt.get(['assignmentNumber'], function (err, result) {
		// Based on the number function gets called
		switch(result.assignmentNumber) {
				case '1' : diamond ();
									 break;
				case '2' : fibonacci ();
									 break;
				case '3' : removeReverse ();
									 break;
				case '4' : studentDetails ();
									 break;
				case '5' : authorBooks ();
									 break;
				default  : console.log("Enter a valid value");
									 return(false);	
		}
	});
};

//function to prompt the user to input the diamond length
var diamond = function () {
	prompt.start(); 
	prompt.get(['length'], function (err, result) {
		length=Math.round(result.length);	
//Exception handling to check if the number is odd or even	and the maximum number of rows	
	});
};
//Function to print the diamond
var drawDiamond = function (length) {
	try { 
			if(length % 2 === 0){ 
					throw new Error("Wrong value");
			}
			else {
				var k = 1, c = 1, space = length - 1, str="";
				for (k = 1; k <= length; k++,space--)	{
					for (c = 1; c <= space; c++){
						str = str + " ";
					}
					for (c = 1; c <= 2*k-1; c++){
						str = str + "*";
					}
					str = str + "\n";
				}
				space = 1;
				for (k = 1; k <= length -1; k++) {
					for ( c=1; c <= space ; c++){
						str = str +" ";
					}
					space++;
					for ( c=1; c <= 2* ( length -  k) -1; c++){
						str = str + "*";
					}
					str=str + "\n";
				}
				console.log(str);	
				return 0;
			}
		}	catch(err) {
			console.log(err);
			length = 0;
//			diamond();
		}	
};
//function to prompt the user to input the fibonnaci length
var fibonacci = function () {
	var fibonacciFlag;
	console.log("enter an series length");
	prompt.start();
	prompt.get(['length'], function (err, result) {
//rounding off the value in case of float number
		length= Math.round(result.length);
		try {
			if(isNaN(result.length) ===true)
			{
				throw "Enter a valid integer";
			}
			else {
				calcFibonacci(result.length);	
				displayMenu();
			}
		}
		catch(error)
		{
			console.log(error);
			fibonacci();
		}
	});
};
// Function to calculate fibonacci Series
var calcFibonacci = function (length) {
		var number = length, a =0, b =1, c =0, fib=0,j=0,arr=[];
		if(number==1) {
			fib=0;
			arr.push(fib);
		} else if(number==2) {
			arr.push(a);
			arr.push(b);
		}	else if(number === 0) {
			console.log("Enter a number greater than 0");
		} else {
			arr.push(a);
			arr.push(b);
			for(j=2;j<number;j++) {
				c=a+b;
				arr.push(c);
				a=b;
				b=c;		
		}
		
}
		console.log(arr);
			return arr;
};
var removeReverse = function () {
	var str;
	prompt.start(); 
	//asks the user to input the assignment number
	prompt.get(['string'], function (err, result) {
		str = result.string;
		reverseString(str);
		displayMenu();
	});
};
var reverseString = function (str) {
	var strConsonants ;	
	//global regex to check for vowels
	strConsonants = str.replace(/[aeiou]/gi, '');
	// '/s' regex for space and /g represents global checking
	strConsonants = strConsonants.replace(/\s+/g, '');
	var reverse= strConsonants.split("").reverse().join("");
	console.log("The Reversed String "+reverse);
	return reverse;
};

//sorting the student array
var studentDetails = function () {
	var student = [ 
	{"id" : '103',"name": 'John',"age" : '20'},
	{	"id" : '105',"name": 'Henry',"age" : '30'},
	{ "id" : '101',"name": 'Harry',"age" : '24'},
	{ "id" : '104',"name": 'Fedrick',"age" : '29'},
	{ "id" : '102',"name": 'Cooper',"age" : '31'}	];
	console.log("Enter the order to sort: 1.Ascending 2. Descending");
	prompt.start(); 
	//asks the user to input the assignment number
	prompt.get(['option'], function (err, result) {
		var option=result.option;
		studentSort(option, student);
		displayMenu();
	});
};

var studentSort = function (option,studentsort) {
	var studentnew;
	console.log("The Student Array");
	console.log(studentsort);
	if(option === '1')
		{
			studentnew=studentsort.sort(function(obj1,obj2)
			{
				return obj1.id-obj2.id;
			});
		} else {
			studentnew=studentsort.sort(function(obj1,obj2)
			{
				return obj2.id-obj1.id;
			});
		} 
		console.log(studentnew);
		return studentnew;
};
var authorBooks = function () {
	var useroption ;
	console.log("Enter the operation to be performed");
	console.log("1. Add books  2.Add Authors 3.Search");
	prompt.start();
	prompt.get(['option'], function (err, result) {
		useroption = result.option;
		switch (useroption)
		{
				case '1': addBook();
				          break;
				case '2': addAuthor();
				          break;
				case '3': searchbook();
		}
	});
};
var addBook = function () {
	console.log("Enter the  author name");
	prompt.start ();
	prompt.get (['auname'],function (err, result) {
	checkauthor(result.auname);
	});
};

var checkauthor = function (name) {
	var authorName = name;
  var j=0; 
		for (j=0;j<bookObj.length; j++)
		{
			if(bookObj[j].author.toLowerCase() === authorName)
			{	
				console.log("Enter the book name" );
				prompt.start();
			 	prompt.get (['book'],function (err, result) {
			 	bookObj[j].books.push(result.book);
					console.log(bookObj);
					return '1';
					
			});
				break;
			} else {
				console.log("The Author is not available ");
//				authorBooks();
				return '2';
			}
		}	
};
var addAuthor = function () {
	console.log("Enter the  author name");
	prompt.start ();
	prompt.get (['name'],function (err, result) {
	checkName(result.name);
	});	
};
var checkName = function (name) {
	var authorName = name;
  try{
		for (var i in bookObj)
		{	
			if(bookObj[i].author.toLowerCase() === authorName)
			{
				throw "The Author is already available";
			}
		 else {
		 	bookObj.push({"author" :name,"book":""}); 
			 console.log(bookObj);
			 return '1';
			 break;
		 }
		}
	}
	catch(err)
	{
		console.log(err);
		addAuthor();
	}				 
};

var searchbook = function() {
	console.log("Enter the  book name to search");
	prompt.start ();
	prompt.get (['searchbook'],function (err, result) {
	search(result.searchbook);
	displayMenu();
	});
};
var search = function(searchbook) {
	for (var i in bookObj) {	
		for (var j in bookObj[i].books) {
			if(bookObj[i].books[j].toLowerCase().indexOf(searchbook) !== -1 ) {
				console.log(bookObj[i]);
			}
		}
	}	
	return 1;
}

// calling the menu function
displayMenu();
module.exports.drawDiamond = drawDiamond;
module.exports.calcFibonacci = calcFibonacci;
module.exports.reverse = reverseString;
module.exports.studentSort = studentSort;
module.exports.search = search;