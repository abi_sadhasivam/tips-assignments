/* global require:true */
/* global describe:true */
/* global module:true */
/* global it:true */
var moduleFunction = require('./prompt.js');
var sortArray = [ 
	{"id" : '103',"name": 'John',"age" : '20'},
	{	"id" : '105',"name": 'Henry',"age" : '30'},
	{ "id" : '101',"name": 'Harry',"age" : '24'},
	{ "id" : '104',"name": 'Fedrick',"age" : '29'},
	{ "id" : '102',"name": 'Cooper',"age" : '31'}	];
var sortedArray = [
	{ "id" : '101',"name": 'Harry',"age" : '24'},
	{ "id" : '102',"name": 'Cooper',"age" : '31'},
	{ "id" : '103',"name": 'John',"age" : '20'},
	{ "id" : '104',"name": 'Fedrick',"age" : '29'},
	{	"id" : '105',"name": 'Henry',"age" : '30'}
];
var assert = require("assert");
describe('#Draw diamond', function(){
	it('the flag value shoulb in 0', function(){
			assert.equal(0,moduleFunction.drawDiamond(3));
	});

	it('the flag value shoulb in false', function(){
		assert.throws(function() {
			moduleFunction.drawDiamond(2);
		},Error	);
		});
});
describe('#fibonacci', function(){
	it('fibonacci of the entered value wil be written', function(){
			assert.deepEqual([0,1,1],moduleFunction.calcFibonacci(3));
			assert.deepEqual([0],moduleFunction.calcFibonacci(1));
	});
});
describe('#reverse', function(){
	it('the input string wil be reversed', function(){
			assert.deepEqual("djldsjdsjdsjdkdhskjd",moduleFunction.reverse('djkashd akdj asdja sdjs daljd'));
	});
});

describe('#sort', function(){
	it('the input string wil be reversed', function(){
			assert.deepEqual(sortedArray,moduleFunction.studentSort('1',sortArray));
	});
});
describe('#search', function(){
	it('searching for the book', function(){
			assert.equal('1',moduleFunction.search('rage'));
	});
});